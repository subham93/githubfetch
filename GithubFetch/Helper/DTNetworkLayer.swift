//
//  DTNetworkLayer.swift
//  GithubFetch
//
//  Created by Subham Khandelwal on 18/04/18.
//  Copyright © 2018 DTalk. All rights reserved.
//


import Foundation
import Alamofire
import UIKit

struct BaseModel {
    var message : String?
    var statusCode : Int?
}

class DTNetworkLayer : NSObject {
    
    class func requestGETURL(_ strURL: String, headers : [String : String]?, success:@escaping (Alamofire.DataResponse<Any>) -> Void, failure:@escaping (BaseModel) -> Void) {
        print("====REQUEST:"+strURL)
        Alamofire.request(strURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            self.handleResponse(responseObject: responseObject, success: success, failure: failure)
        }
    }
    
    class func requestPOSTURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (Alamofire.DataResponse<Any>) -> Void, failure:@escaping (BaseModel) -> Void){
        print("====REQUEST:"+strURL+"\nHeaders:\n"+String(describing: headers))
        print("\nParams:"+String(describing: params))
        Alamofire.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            self.handleResponse(responseObject: responseObject, success: success, failure: failure)
        }
    }
    
    class func handleResponse(responseObject : Alamofire.DataResponse<Any>, success:@escaping (Alamofire.DataResponse<Any>) -> Void, failure:@escaping (BaseModel) -> Void) {
        print(responseObject)
        
        if responseObject.result.isSuccess {
            if (responseObject.response?.statusCode == 200 || responseObject.response?.statusCode == 201 ||
                responseObject.response?.statusCode == 202 || responseObject.response?.statusCode == 204) {
                success(responseObject)
            } else {
                failure(self.getFailureMessageJSON(responseObject: responseObject))
            }
        } else {
            failure(self.getFailureMessageJSON(responseObject: responseObject))
        }
    }
    
    class func getFailureMessageJSON(responseObject : Alamofire.DataResponse<Any>) -> BaseModel {
        var failureMsg = DTConstants.failureMessage
        if let networkFailMsg = responseObject.result.error?.localizedDescription {
            failureMsg = networkFailMsg
        }
        print("Maybe Err: " + responseObject.result.value.debugDescription)
        print("Error: " + failureMsg)
        var baseModel = BaseModel()
        baseModel.message = failureMsg
        baseModel.statusCode = responseObject.response?.statusCode
        return baseModel
    }
}
