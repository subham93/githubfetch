//
//  DTConstants.swift
//  GithubFetch
//
//  Created by Subham Khandelwal on 18/04/18.
//  Copyright © 2018 DTalk. All rights reserved.
//

import Foundation

struct DTConstants {
    static let BaseURL = "https://api.github.com/"
    static let searchURL = "search/users?sort=followers&q="
    static let failureMessage = "Something went wrong!"
}
