//
//  DTHelperClass.swift
//  GithubFetch
//
//  Created by Subham Khandelwal on 18/04/18.
//  Copyright © 2018 DTalk. All rights reserved.
//

import Foundation
import UIKit

class DTHelperClass {
    
    class func showAlertOnVC(parentVC : UIViewController, title : String, subtitle : String) {
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        parentVC.present(alert, animated: true, completion: nil)
    }
}
