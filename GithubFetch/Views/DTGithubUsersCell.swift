//
//  DTGithubUsersCell.swift
//  GithubFetch
//
//  Created by Subham Khandelwal on 18/04/18.
//  Copyright © 2018 DTalk. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class DTGithubUsersCell : UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    
    func configureUI(_ userDetails : Users) {
        userNameLbl.text = userDetails.login
        self.imgView.sd_setImage(with: URL(string: userDetails.avatar_url!), placeholderImage: UIImage(named: "placeholder-image.jpg"))
    }
}
