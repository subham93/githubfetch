//
//  ViewController.swift
//  GithubFetch
//
//  Created by Subham Khandelwal on 18/04/18.
//  Copyright © 2018 DTalk. All rights reserved.
//

import UIKit

class DTGithubViewController: DTBaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var usersTableView: UITableView!
    
    var usersArr = [Users]()
    let reuseID = "DTGithubUsersCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureUI()
        self.getGithubUsers(with: "")
    }
    
    func configureUI() {
        
        usersTableView.register(UINib(nibName: "DTGithubUsersCell", bundle: nil), forCellReuseIdentifier: reuseID)
        usersTableView.tableFooterView = UIView()
    }
    
    func getGithubUsers(with query : String) {
        if query.isEmpty {
            self.usersArr.removeAll()
            self.usersTableView.reloadData()
            return
        }
        self.showProgressView()
        let url = DTConstants.BaseURL + DTConstants.searchURL + query
        DTNetworkLayer.requestGETURL(url, headers: nil, success: { [weak self] (responseObject) in
            guard let `self` = self else { return }
            self.hideProgressView()
            self.usersArr.removeAll()
            self.usersTableView.reloadData()
            do {
                let githubUsers : DTGithubUsers = try JSONDecoder().decode(DTGithubUsers.self, from: responseObject.data!) // Decoding our data
                self.usersArr = githubUsers.items
                self.handleData()
            } catch let myError {
                print("caught: \(myError)")
                DTHelperClass.showAlertOnVC(parentVC: self, title : "", subtitle : DTConstants.failureMessage)
            }
            }, failure: { (error) in
                self.hideProgressView()
                if let msg = error.message {
                    DTHelperClass.showAlertOnVC(parentVC: self, title: "", subtitle: msg)
                }
        })
        
    }
    
    func handleData() {
        if usersArr.count > 0 {
            self.usersTableView.reloadData()
        } else {
            DTHelperClass.showAlertOnVC(parentVC: self, title: "", subtitle: "No Records Found!")
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseID) as! DTGithubUsersCell
        cell.configureUI(usersArr[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // Search Bar
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.getGithubUsers(with: searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.getGithubUsers(with: "")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

