//
//  FPGithubUsers.swift
//  GithubFetch
//
//  Created by Subham Khandelwal on 18/04/18.
//  Copyright © 2018 DTalk. All rights reserved.
//

import Foundation

struct DTGithubUsers : Codable {
    var items : [Users]
}

struct Users : Codable {
    
    var login : String?
    var id : Int?
    var avatar_url : String?
    var url : String?
    var followers_url : String?
    var following_url : String?
    var score : Double?
}
